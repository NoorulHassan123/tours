import React from 'react';
import { Container } from 'react-bootstrap';

const Footer = () => {
    return (
        <footer className="footer">
            <Container>
                <div className="footer__wrapper">
                    <span>Copyright &copy; All Rights Reserved.</span>
                    <div className="footer__icon-list">
                        <a className="footer__icon-link" href="https://www.facebook.com/subkafaida/" target="__blank">


                        </a>

                        <a className="footer__icon-link" href="https://www.linkedin.com/company/subkafaida/" target="__blank">

                        </a>

                        <a className="footer__icon-link" href=" https://twitter.com/subkafaida1/" target="__blank">

                        </a>

                        <a className="footer__icon-link" href="https://www.instagram.com/subkafaida/" target="__blank">

                        </a>

                        <a className="footer__icon-link" href="https://www.youtube.com/channel/UCPnKHmwzBj5sN7ULEl5CqNw/" target="__blank">

                        </a>
                    </div>
                </div>
            </Container>
        </footer>
    )
}

export default Footer;
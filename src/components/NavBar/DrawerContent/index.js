import React, { useContext } from 'react';
import { Nav, Navbar, Button, Container, Row, Col, Card } from 'react-bootstrap';
import classnames from 'classnames';
import tabs from '../../../constants/tabs';

const About = () => {
    return (
        <section
            className={classnames('section-about')}
        >
            <Container>
                <Row>
                    <Col className="text-center p-4">
                        <h3>Mission</h3>
                        Apricot Tours (Government Licence: ID-2131) operate in Pakistan. The company’s expertise particularly lies in tours, treks and also expedition assistance in the northern parts of Pakistan. Moreover it is important to mention that the company is equally involved in tourism-related activities throughout Pakistan with the same volume.
                    </Col>
                    <Col className="text-center p-4">
                        <h3>Vision</h3>
                        We at Apricot Tours understand the changing travel patterns and importance of plans/itineraries that are compatible with international routes for on-the-go travelers. Our experienced staff is the best in realizing and responding to the needs of our guest for customized plans.
                    </Col>
                </Row>
            </Container>
        </section>
    )
}

const Philosophy = () => {
    return (
        <section
            className={classnames('section-philosophy')}
        >
            this is PHILOSOPHY
        </section>
    )
}

const Services = () => {
    return (
        <section
            className={classnames('section-services')}
        >
            <Container>
                this is services
            </Container>
        </section>
    )
}

const Works = () => {

    return (
        <section
            className={classnames('section-services')}
        >
            this is works

        </section>
    )
}

const DrawerContent = ({ tab }) => {
    switch (tab) {
        case tabs.ABOUT:
            return <About />;
        case tabs.PHILOSOPHY:
            return <Philosophy />;
        case tabs.SERVICES:
            return <Services />;
        case tabs.WORK:
            return <Works />;
        default:
            return <></>;
    }
}

export default DrawerContent;
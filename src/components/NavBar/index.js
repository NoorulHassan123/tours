import React, { useState, useEffect, useContext } from 'react';
import { Nav, Navbar, Button, Container, Row, Col, Card, NavDropdown } from 'react-bootstrap';
import { Link, useHistory } from 'react-router-dom';
import classnames from 'classnames';
import logoBranding from '../../images/tours-logo.jfif'
import DrawerContent from './DrawerContent';
import userAvatar from "../../images/iconfinder_user.png";

import tabs from '../../constants/tabs';
import { AuthContext } from '../../contexts/auth';

const NavBar = ({ onRegisterClick }) => {
    // const { isLogin, signOut, profile, isProfileImg } = useContext(AuthContext);
    const [userMenu, setUserMenu] = useState(false);
    const [activeTab, setActiveTab] = useState('');
    const { isLogin, loginSuccess, isProfileImg, setIsProfileImg, signOut, profile } = useContext(AuthContext)
    console.log('isProfileImg', isProfileImg)
    const history = useHistory();

    const onHomeChange = () => {
        setActiveTab(activeTab === tabs.HOME ? '' : tabs.HOME);
        history.push('/');
    }

    const onNavChange = (route) => {
        setActiveTab(route);
        history.push(`/${route}`);
    }
    const toggle = () => {
        setUserMenu(!userMenu);
    };
    return (
        <>
            <Navbar className="navigation shadow" bg="light" expand="lg">
                <Container>
                    <Navbar.Brand as={Link} to="/">
                        <img className="navigation__logo" src={logoBranding} alt="logo" />
                    </Navbar.Brand>
                    <Navbar.Toggle aria-controls="basic-navbar-nav" />
                    <Navbar.Collapse id="basic-navbar-nav">
                        <Nav className="ml-auto" style={{ width: '100%' }}>
                            <Nav.Link
                                className={classnames("navigation__link", { ['navigation__link--active']: activeTab === tabs.HOME && history.location.pathname === '/' })}
                                onClick={() => onHomeChange()}
                            >
                                Home
                            </Nav.Link>
                            <Nav.Link
                                className={classnames("navigation__link", { ['navigation__link--active']: activeTab === tabs.ABOUT })}
                                onClick={() => setActiveTab(activeTab === tabs.ABOUT ? '' : tabs.ABOUT)}
                            >
                                About
                            </Nav.Link>

                            {/* {isLogin ?
                                <Nav.Link
                                    className={classnames("navigation__link", { ['navigation__link--active']: activeTab === 'wallet' || history.location.pathname === '/wallet' })}
                                    onClick={() => onNavChange('wallet')}
                                >
                                    Wallet
                                </Nav.Link> : ""
                            } */}

                            {/* {isLogin ? (
                                <Button className="button-ghost" style={{ marginLeft: "1rem" }} variant="outline-light" onClick={() => signOut()}>Sign out</Button>
                            ) :
                                <Button className="button-ghost" style={{ marginLeft: "1rem" }} variant="outline-light" onClick={() => history.push('/signin')}>Sign In</Button>
                            } */}

                            {isLogin ? (
                                <>
                                    <a
                                        className="navigation__user-container"
                                        aria-haspopup="false"
                                        aria-expanded="false"
                                        type="button"
                                        onClick={toggle}
                                        style={{ marginLeft: 'auto' }}
                                    >
                                        {isProfileImg ? (
                                            <img
                                                className="navbar__user-img"
                                                src={profile.profileImage}
                                                alt="profile image"
                                                style={{ width: "100%", height: "100%", borderRadius: "45%" }}
                                            />
                                        ) : (
                                            <img
                                                className="navigation__user-img"
                                                src={userAvatar}
                                                alt="profile img"
                                                style={{ width: "100%", height: "100%" }}
                                            />
                                        )}



                                    </a>
                                    <NavDropdown
                                        id={`dropdown-button-drop-down`}
                                        // id="basic-nav-dropdown"
                                        onClick={toggle}
                                        show={userMenu}
                                        drop='down'
                                    >
                                        {/* <NavDropdown.Item style={{ fontSize: "14px" }} disabled>
                                    {` ${currentUser.firstName} ${currentUser.lastName}`}
                                </NavDropdown.Item> */}
                                        <NavDropdown.Item
                                            as={Link}
                                            style={{ fontSize: "14px" }}
                                            to="/profile"
                                        >
                                            Profile
                                        </NavDropdown.Item>
                                        <NavDropdown.Divider />
                                        <NavDropdown.Item
                                            as={Link}
                                            style={{ fontSize: "14px" }}
                                            to="#"
                                            onClick={() => {
                                                signOut();
                                                setIsProfileImg(false);
                                                history.push("/")
                                            }}
                                        >
                                            Logout
                                        </NavDropdown.Item>
                                    </NavDropdown>
                                </>
                                // <Button className="button ml-4" onClick={() => signOut()}>Sign Out</Button>
                            ) : (
                                <Button className="button-ghost ml-auto" style={{ marginLeft: 'auto' }} variant="outline-light" onClick={() => onNavChange('signIn')}>Sign In</Button>
                            )}
                        </Nav>

                    </Navbar.Collapse>
                </Container>

                <div className="navigation__drawer" onMouseLeave={() => setActiveTab('')}>
                    <Container>
                        <div
                            className={classnames('navigation__drawer-item', {
                                ['d-block']: activeTab !== ''
                            })}
                        >
                            <DrawerContent tab={activeTab} />
                        </div>
                    </Container>
                </div>
            </Navbar>
        </>
    );
}

export default NavBar;
const initialState = {
    tours: [],
    tour: [],
};

const tourReducer = (state = initialState, action) => {
    switch (action.type) {
        case "GET_ALL_TOURS":
            return {
                ...state,
                tours: action.payload
            };
        case "GET_TOUR":
            return {
                ...state,
                tour: action.payload.doc
            };
        default:
            return state;

    }
}
export default tourReducer;
import api from '../../api';

export const createUser = (data) => {
    return async (dispatch) => {
        const res = await api.post("/users/signup", data);
        console.log("res", res)
        dispatch({
            type: "CREATE_USER",
            payload: res.user
        });
    };
};

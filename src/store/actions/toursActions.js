import api from '../../api';


export const getAllTours = () => {
    return async (dispatch) => {
        const tours = await api.get("/tours");
        console.log("res", tours)
        dispatch({
            type: "GET_ALL_TOURS",
            payload: tours.data.docs
        });
    };
};

export const getTour = (id) => {
    return async (dispatch) => {
        const tour = await api.get(`tours/${id}`);
        console.log("res", tour)
        dispatch({
            type: "GET_TOUR",
            payload: tour.data
        });
    };
};
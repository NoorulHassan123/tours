import React, { useContext, useEffect, useState } from 'react'
import { useParams } from 'react-router';
import { getTour } from "../store/actions/toursActions"
import { bookTour } from "../store/actions/paymentActions"
import { useDispatch, useSelector } from 'react-redux';
import { Row, Container, Col, Card, Button, Spinner } from 'react-bootstrap';
import api from '../api';
import { loadStripe } from "@stripe/stripe-js";
import { Carousel } from 'react-responsive-carousel';
import { AuthContext } from '../contexts/auth';


const MyCarousel = () => {
    return (
        <Carousel
            className="my-carousel"
            // style={{ height: '10%' }}
            showArrows={true}
            showStatus={false}
            showIndicators={false}
            showThumbs={false}
            interval={3000}
            infiniteLoop={true}
            autoPlay={true}
        >
            <div>
                <img src='https://www.apricottours.pk/wp-content/uploads/2019/10/K2-Base-Camp-via-Karakoram-Highway-8.jpg' />
                {/* <p className="legend">Restaurants</p> */}
            </div>
            <div>
                <img src='https://www.apricottours.pk/wp-content/uploads/2017/03/phander-lake-ghizer-gilgit-baltistan-1.jpg' />
                {/* <p className="legend">Hotels</p> */}
            </div>
            <div>
                <img src='https://www.apricottours.pk/wp-content/uploads/2017/03/kharmang-manthoka-waterfall.jpg' />
                {/* <p className="legend">Clothing and Fashion</p> */}
            </div>
            <div>
                <img src='https://www.apricottours.pk/wp-content/uploads/2017/04/rakaposhi-bc-tagafari.jpg' />
                {/* <p className="legend">Pharmacy</p> */}
            </div>
        </Carousel>
    )
}



const TourDetail = () => {
    const { id } = useParams();
    const dispatch = useDispatch();

    const [isLoading, setIsLoading] = useState(false)
    const { isLogin, loginSuccess } = useContext(AuthContext)

    const tour = useSelector((state) => {
        return state.tours.tour;
    });

    const onBookTourClick = async (tourId) => {
        try {
            setIsLoading(true);
            const session = await api.get(`/bookings/checkout-session/${tourId}`);
            console.log("session", session);
            const stripePromise = loadStripe(
                "pk_test_51JeHNnDks1a4a4VZxsO1kcXvN8B80ClSr20p4FgP2cg7O5lCgiDZKP1hwltbNi7LGCh3TKliyDgHIQ0L8H1x2qhD00Vf9Oe7Ld"
            );
            const stripe = await stripePromise;
            await stripe.redirectToCheckout({
                sessionId: session.data.session.id
            })
            setIsLoading(false);

        }
        catch (err) {
            console.log(err)
        }
        // dispatch(bookTour(id));
    }

    useEffect(() => {
        dispatch(getTour(id));
        window.scrollTo(0, 0);
    }, [])
    return (

        <Container className="section-tourDetail">
            <Row>
                {/* <Col> */}
                <Card style={{ margin: "auto", padding: "1rem", marginTop: "2rem", }}>
                    <Card.Img variant="top" src={tour.image} style={{ height: '20rem' }} />
                    <Card.Body style={{ display: "flex", flexDirection: "column", justifyContent: "space-between" }}>
                        <Card.Title className='tourDetail_title'>{tour.place}</Card.Title>
                        <Card.Text className='mb-4'>
                            {tour.discription}
                            Tours sits on the lower reaches of the Loire, between Orléans and the Atlantic coast. Formerly named Caesarodunum by its founder, Roman Emperor Augustus, it possesses one of the largest amphitheaters of the Roman Empire, the Tours Amphitheatre. Known for the Battle of Tours in 732 CE, it is a National Sanctuary with connections to the Merovingians and the Carolingians, with the Capetians making the kingdom's currency the Livre tournois. Saint Martin, Gregory of Tours and Alcuin were all from Tours.
                        </Card.Text>
                        <div style={{ width: '80%', height: '50%', margin: 'auto' }}>

                            <MyCarousel />
                        </div>
                    </Card.Body>
                    {
                        isLogin ?
                            <Button disabled={isLoading ? true : false} className='bookNow_btn' onClick={() => onBookTourClick(tour._id)} >
                                {isLoading ? (
                                    <div>
                                        <Spinner animation="border" size="sm" />
                                        {/* <span style={{ marginLeft: '10px' }}>Book Now</span> */}
                                    </div>
                                ) :
                                    <span>Book Now</span>
                                }
                            </Button>
                            :
                            <Button disabled={true} className='bookNow_btn'>
                                <span>Signin to Book Tour</span>
                            </Button>
                    }
                </Card>
                {/* </Col> */}

            </Row>
        </Container >

    )
}

export default TourDetail;

import React, { useEffect, useState } from 'react';
import { Button, Card, Container } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import { getAllTours } from "../store/actions/toursActions";
import { Link } from 'react-router-dom';

const Home = () => {
    const dispatch = useDispatch();
    // const [tours, setTours] = useState([]);
    const tours = useSelector((state) => {
        return state.tours.tours;
    });

    useEffect(() => {
        console.log("FDs")
        dispatch(getAllTours());
        window.scrollTo(0, 0);
    }, [])
    return (
        <div className="section-home">
            <Container className="d-flex" style={{ flexWrap: "wrap" }}>
                {
                    tours.map((tour) => {
                        return (
                            <Card style={{ width: '18rem', margin: "1rem 1rem", cursor: 'pointer' }} className='shadow' >
                                <Card.Img variant="top" src={tour.image} style={{ height: "200px" }} />
                                <Card.Body style={{ display: "flex", flexDirection: "column", justifyContent: "space-between" }}>
                                    <Card.Title>{tour.place}</Card.Title>
                                    <Card.Text>
                                        {tour.discription} <br />
                                        <b style={{ float: 'right', color: '#ff8100', fontSize: '1.8rem' }}>{tour.fee}$</b>

                                    </Card.Text>
                                    <Link to={`/tours/${tour._id}`} >
                                        <Button variant="primary" style={{ width: '-webkit-fill-available' }}>Details</Button>
                                    </Link>
                                </Card.Body>
                            </Card>
                        )
                    })
                }
            </Container>
        </div>
    )
}

export default Home

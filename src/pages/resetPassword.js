import React, { useState, useEffect, useContext } from 'react';
import { Row, Col, Form, Container, Button, Alert, Tabs, Tab, Spinner, Modal } from 'react-bootstrap';
import { Link, useHistory } from 'react-router-dom';
import { Formik } from 'formik';
import * as Yup from 'yup';
import api from './../api/index';
import { AuthContext } from '../contexts/auth';
import { useParams } from "react-router-dom";


const ResetPassword = () => {

    const validSchema = Yup.object().shape({
        // oldPassword: Yup.string()
        //     .min(8, "Your old password must be at least 8 characters long").max(100, "Too long")
        //     .required("Required")
        //     .matches(
        //         /^(?=.*[a-z])(?=.*[@$!%*#?&])(?=.*[0-9]).*$/,
        //         "Must contain at least 1 lowercase character, 1 number, 1 special character"
        //     ),

        password: Yup.string().required("Required"),
        confirmPassword: Yup.string().required('required').oneOf([Yup.ref("password"), null], "Passwords must match"),
    });

    const { isLogin, loginSuccess, setProfile, setIsProfileImg, profile } = useContext(AuthContext);
    const history = useHistory();

    const [resetPasswordSuccesAlerts, setResetPasswordSuccesAlerts] = useState(false);
    const [resetPasswordDangerAlerts, setResetPasswordDangerAlerts] = useState(false);
    const [isLoading, setIsLoading] = useState(false);

    const { resetPasswordToken } = useParams();
    console.log('resetPasswordToken', resetPasswordToken);

    const handleResetPasswordSubmit = async (values, clearForm) => {
        setIsLoading(true);
        console.log('values', values);
        try {
            const res = await api.patch(`users/resetPassword/${resetPasswordToken}`, values);
            console.log(res);
            setResetPasswordSuccesAlerts(true);
            setResetPasswordDangerAlerts(false);
            setIsLoading(false);
        } catch (error) {
            console.log(error);
            setResetPasswordSuccesAlerts(false);
            setResetPasswordDangerAlerts(true);
            setIsLoading(false);

        }
    }


    useEffect(() => {

        window.scrollTo(0, 0);
    }, [])
    return (
        <Container className="section-signIn">
            <Row>
                <Col className='shadow' md={4} style={{ padding: "1rem", margin: 'auto', marginTop: '2rem' }}>
                    <Formik
                        onSubmit={(values, { resetForm }) => {
                            handleResetPasswordSubmit(values, resetForm)
                            // resetForm({ values: "" })
                        }}
                        validationSchema={validSchema}
                        enableReinitialize
                        initialValues={{
                            // oldPassword: '',
                            password: '',
                            confirmPassword: '',
                        }}
                    >
                        {(formik) => (
                            <Form onSubmit={formik.handleSubmit} id="resetPassword">
                                <div className="card-body shadow-2">
                                    <div className="mb-4 text-center">
                                        <i className="feather icon-unlock auth-icon" />
                                    </div>
                                    <h3 className="mb-4 text-center">Reset Password</h3>
                                    {
                                        resetPasswordSuccesAlerts ?
                                            <Alert variant="success" className="rounded text-center">Your Password has been Reset Successfully</Alert>
                                            : ""
                                    }
                                    {
                                        resetPasswordDangerAlerts ?
                                            <Alert variant="danger" className="rounded text-center">Incorrect Password!</Alert>
                                            : ""
                                    }

                                    {
                                        resetPasswordSuccesAlerts ?
                                            ""
                                            :
                                            <>
                                                <Form.Group controlId="password" as={Col} className="isValidHide" hasValidation>
                                                    <Form.Label className="form__label">New Password</Form.Label>

                                                    <Form.Control
                                                        className="p-3 rounded-0 "
                                                        // style={{backG}}
                                                        type="password"
                                                        name="password"
                                                        placeholder="Please Enter Your New Password"
                                                        value={formik.values.password}
                                                        onChange={formik.handleChange}
                                                        onBlur={formik.handleBlur}
                                                        isValid={formik.touched.password && !formik.errors.password}
                                                        isInvalid={formik.touched.password && formik.errors.password}
                                                    />

                                                    <Form.Control.Feedback type="invalid">
                                                        {formik.errors.password}
                                                    </Form.Control.Feedback>
                                                </Form.Group>

                                                <Form.Group controlId="confirmPassword" as={Col} className="isValidHide" hasValidation>
                                                    <Form.Label className="form__label">Confirm Password</Form.Label>

                                                    <Form.Control
                                                        className="p-3 rounded-0 "
                                                        // style={{backG}}
                                                        type="password"
                                                        name="confirmPassword"
                                                        placeholder="Please Enter Your Confirm Password"
                                                        value={formik.values.confirmPassword}
                                                        onChange={formik.handleChange}
                                                        onBlur={formik.handleBlur}
                                                        isValid={formik.touched.confirmPassword && !formik.errors.confirmPassword}
                                                        isInvalid={formik.touched.confirmPassword && formik.errors.confirmPassword}
                                                    />

                                                    <Form.Control.Feedback type="invalid">
                                                        {formik.errors.confirmPassword}
                                                    </Form.Control.Feedback>
                                                </Form.Group>
                                            </>
                                    }

                                    <div style={{ textAlign: 'center' }}>
                                        {
                                            resetPasswordSuccesAlerts ?
                                                <Button style={{ marginLeft: '1rem' }}>
                                                    < Link to='/signIn' style={{ color: 'beige' }}>
                                                        Go to Signin
                                                    </Link>
                                                </Button>
                                                :
                                                <Button style={{ marginLeft: '1rem' }} type="submit">
                                                    {isLoading ? (
                                                        <Spinner animation="border" size="sm" />
                                                    ) : ('')}
                                                    <span className={`${isLoading ? 'ml-2' : ''}`}>Submit</span>
                                                </Button>
                                        }
                                    </div>
                                </div>
                            </Form>
                        )}
                    </Formik>
                </Col>
            </Row>
        </Container >

    )
}

export default ResetPassword;

import React, { useState, useEffect, useContext } from 'react';
import { Row, Col, Form, Container, Button, Alert, Tabs, Tab, Spinner } from 'react-bootstrap';
import { Link, useHistory } from 'react-router-dom';
import { Formik } from 'formik';
import * as Yup from 'yup';
import { createUser } from '../store/actions/authActions'
import { useDispatch } from 'react-redux';
import api from '../api';
const SignUp = () => {
    const validSchema = Yup.object().shape({
        name: Yup.string().required("required"),
        email: Yup.string().email().required("required"),
        phone: Yup.string().required("required"),
        address: Yup.string().required("required"),
        password: Yup.string().required("required"),
        confirmPassword: Yup.string().required("required"),
    });
    const dispatch = useDispatch();
    const history = useHistory();
    const [isLoading, setIsLoading] = useState(false)


    const onSubmitSignUp = async (values) => {
        setIsLoading(true);
        console.log(values)
        try {
            // dispatch(createUser(values));
            const res = await api.post('users/signup', values);
            console.log(res);
            // loginSuccess(res.data.token);
            history.push('/signin')
            setIsLoading(false);

        } catch (error) {
            console.log(error)
            setIsLoading(false);
        }

    }
    return (
        <Container className="section-signUp shadow" style={{ marginTop: '8rem', minHeight: '60vh' }}>
            {
                <Formik
                    onSubmit={(values) => onSubmitSignUp(values)}
                    validationSchema={validSchema}
                    enableReinitialize
                    initialValues={{
                        name: '',
                        email: '',
                        phone: '',
                        address: '',
                        password: '',
                        confirmPassword: '',
                    }}
                >
                    {(formik) => (
                        <Form onSubmit={formik.handleSubmit} id="signUp">
                            <Form.Row>

                                <Form.Group controlId="name" as={Col} hasValidation>
                                    <Form.Label className="form__label">Name</Form.Label>
                                    <Form.Control
                                        className="p-3 rounded-0"
                                        type="text"
                                        name="name"
                                        placeholder="Enter Your Name"
                                        value={formik.values.name}
                                        onChange={formik.handleChange}
                                        onBlur={formik.handleBlur}
                                        isValid={formik.touched.name && !formik.errors.name}
                                        isInvalid={formik.touched.name && formik.errors.name}
                                    />
                                    <Form.Control.Feedback type="invalid">
                                        {formik.errors.name}
                                    </Form.Control.Feedback>
                                </Form.Group>
                                <Form.Group controlId="email" as={Col} hasValidation>
                                    <Form.Label className="form__label">Email</Form.Label>
                                    <Form.Control
                                        className="p-3 rounded-0"
                                        type="text"
                                        name="email"
                                        placeholder="Enter Your Email"
                                        value={formik.values.email}
                                        onChange={formik.handleChange}
                                        onBlur={formik.handleBlur}
                                        isValid={formik.touched.email && !formik.errors.email}
                                        isInvalid={formik.touched.email && formik.errors.email}
                                    />

                                    <Form.Control.Feedback type="invalid">
                                        {formik.errors.email}
                                    </Form.Control.Feedback>
                                </Form.Group>
                            </Form.Row>
                            <Form.Row>
                                <Form.Group controlId="phone" as={Col} hasValidation>
                                    <Form.Label className="form__label">Phone</Form.Label>
                                    <Form.Control
                                        className="p-3 rounded-0"
                                        type="text"
                                        name="phone"
                                        placeholder="Enter Your Phone"
                                        value={formik.values.phone}
                                        onChange={formik.handleChange}
                                        onBlur={formik.handleBlur}
                                        isValid={formik.touched.phone && !formik.errors.phone}
                                        isInvalid={formik.touched.phone && formik.errors.phone}
                                    />

                                    <Form.Control.Feedback type="invalid">
                                        {formik.errors.phone}
                                    </Form.Control.Feedback>
                                </Form.Group>
                                <Form.Group controlId="address" as={Col} hasValidation>
                                    <Form.Label className="form__label">Address</Form.Label>
                                    <Form.Control
                                        className="p-3 rounded-0"
                                        type="text"
                                        name="address"
                                        placeholder="Please Enter Your Phone Number or Email"
                                        value={formik.values.address}
                                        onChange={formik.handleChange}
                                        onBlur={formik.handleBlur}
                                        isValid={formik.touched.address && !formik.errors.address}
                                        isInvalid={formik.touched.address && formik.errors.address}
                                    />

                                    <Form.Control.Feedback type="invalid">
                                        {formik.errors.address}
                                    </Form.Control.Feedback>
                                </Form.Group>
                            </Form.Row>

                            <Form.Row>
                                <Form.Group controlId="password" as={Col} className="isValidHide" hasValidation>
                                    <Form.Label className="form__label">Password</Form.Label>

                                    <Form.Control
                                        className="p-3 rounded-0 "
                                        // style={{backG}}
                                        type="password"
                                        name="password"
                                        placeholder="Please Enter Your Password"
                                        value={formik.values.password}
                                        onChange={formik.handleChange}
                                        onBlur={formik.handleBlur}
                                        isValid={formik.touched.password && !formik.errors.password}
                                        isInvalid={formik.touched.password && formik.errors.password}
                                    />
                                    <Form.Control.Feedback type="invalid">
                                        {formik.errors.password}
                                    </Form.Control.Feedback>
                                </Form.Group>

                                <Form.Group controlId="confirmPassword" as={Col} className="isValidHide" hasValidation>
                                    <Form.Label className="form__label">Confirm Password</Form.Label>

                                    <Form.Control
                                        className="p-3 rounded-0 "
                                        // style={{backG}}
                                        type="password"
                                        name="confirmPassword"
                                        placeholder="Please Enter Confirm Password"
                                        value={formik.values.confirmPassword}
                                        onChange={formik.handleChange}
                                        onBlur={formik.handleBlur}
                                        isValid={formik.touched.confirmPassword && !formik.errors.confirmPassword}
                                        isInvalid={formik.touched.confirmPassword && formik.errors.confirmPassword}
                                    />
                                    <Form.Control.Feedback type="invalid">
                                        {formik.errors.confirmPassword}
                                    </Form.Control.Feedback>
                                </Form.Group>

                            </Form.Row>
                            <Form.Group as={Col} style={{ display: "flex", justifyContent: "center", alignItems: "center" }}>
                                <Button className="button section-signin__login-btn button btn-block rounded-0" type="submit" style={{ width: '17rem' }} form="signUp">
                                    {isLoading ? (
                                        <div>
                                            <Spinner animation="border" size="sm" />
                                            <span style={{ marginLeft: '10px' }}>Sign Up</span>
                                        </div>
                                    ) :
                                        <span>Sign Up</span>
                                    }
                                </Button>
                            </Form.Group>
                        </Form>
                    )
                    }
                </Formik>
            }
        </Container >

    )
}

export default SignUp;

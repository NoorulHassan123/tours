import React, { useState, useEffect, useContext } from 'react';
import { Row, Col, Form, Container, Button, Alert, Tabs, Tab, Spinner, Modal } from 'react-bootstrap';
import { Link, useHistory } from 'react-router-dom';
import { Formik } from 'formik';
import * as Yup from 'yup';
import api from './../api/index';
import { AuthContext } from '../contexts/auth';
import firebase from "firebase";
// import firebase from 'firebase/compat/app';
import StyledFirebaseAuth from 'react-firebaseui/StyledFirebaseAuth';

const SignIn = () => {

    const validSchema = Yup.object().shape({
        email: Yup.string().email().required("required"),
        password: Yup.string().required("required"),
    });

    const resetPasswordvalidSchema = Yup.object().shape({
        email: Yup.string().email().required("required"),
    });

    const { isLogin, loginSuccess, setProfile, setIsProfileImg, profile } = useContext(AuthContext);
    const history = useHistory();
    const [loginError, setloginError] = useState(false);
    const [isLoading, setIsLoading] = useState(false)
    const [resetPasswordModal, setResetPasswordModal] = useState(false);
    const [isLoadingResetPassword, setIsLoadingResetPassword] = useState(false);
    const [resetPasswordSuccessAlert, setResetPasswordSuccessAlert] = useState(false);
    const [resetPasswordDangerAlert, setResetPasswordDangerAlert] = useState(false);

    const uiConfig = {
        signInFlow: "popup",
        signInOptions: [
            firebase.auth.GoogleAuthProvider.PROVIDER_ID,
            firebase.auth.FacebookAuthProvider.PROVIDER_ID
        ],
        callbacks: {
            signInSuccess: async (currentUser, credential, redirectUrl) => {
                try {
                    console.log("current user", currentUser)
                    // const userId = currentUser.uid;
                    const res = await api.post('users/social/login', {
                        name: currentUser.displayName,
                        email: currentUser.email,
                        socialUserId: currentUser.uid,
                        photo: currentUser.photoURL
                    });

                    loginSuccess(res.data.token);

                    // if (res.data.user.profileImage) {
                    //     loginSuccess(res.data.token);
                    //     setIsProfileImg(true);
                    //     setProfile({ profileImage: res.data.data.user.profileImage })

                    // } else {
                    //     loginSuccess(res.data.token, currentUser.Aa);
                    // }
                    console.log(res.data.user)
                    // setProfile({ name: res.data.user.name, email: res.data.user.email });

                    if (res.data.user.photo !== 'default.png') {
                        console.log('in condition');
                        setIsProfileImg(true);
                        setProfile({ ...profile, name: res.data.user.name, email: res.data.user.email, profileImage: res.data.user.photo });
                    }

                    // settingProfile(res.data.data.user)
                    redirectUrl = history.push('/');
                    return false;
                } catch (err) {
                    console.log('err: ', { ...err })
                    setloginError(true)
                    // if (err.response.status === 409) {
                    // }
                    redirectUrl = history.push('/signin');
                    return false;
                }
            },
        }
    }
    const handleResetPasswordModalClose = () => {
        setResetPasswordModal(false);
        setResetPasswordSuccessAlert(false)
        setResetPasswordDangerAlert(false)
    }

    const onsubmitSignIn = async (values) => {
        setIsLoading(true)
        try {
            const res = await api.post('users/login', values);
            console.log('res signin', res);
            loginSuccess(res.data.token);
            // setProfile({ ...profile, name: res.data.user.name, email: res.data.user.email, phone: res.data.user.phone });

            if (res.data.user.photo !== 'default.png') {
                console.log('in condition')
                setIsProfileImg(true);
                setProfile({ ...profile, name: res.data.user.name, email: res.data.user.email, phone: res.data.user.phone, profileImage: res.data.user.photo });
            } else {
                setProfile({ ...profile, name: res.data.user.name, email: res.data.user.email, phone: res.data.user.phone });
            }
            history.push('/');
            setIsLoading(false);
        } catch (error) {
            console.log(error)
            setIsLoading(false)
            setloginError(true)
        }

    }

    const onResetPasswordEmailSubmit = async (values) => {
        setIsLoadingResetPassword(true)
        try {
            const res = await api.post('users/forgotPassword', values);
            setIsLoadingResetPassword(false);
            setResetPasswordSuccessAlert(true)
            setResetPasswordDangerAlert(false)
        } catch (error) {
            console.log(error);
            setIsLoadingResetPassword(false);
            setResetPasswordSuccessAlert(false);
            setResetPasswordDangerAlert(true);
        }
    }
    useEffect(() => {
        window.scrollTo(0, 0);
    }, [])
    return (
        <Container className="section-signIn">
            <Row>
                <Col className='shadow' md={4} style={{ padding: "1rem", margin: 'auto', marginTop: '2rem' }}>
                    {/* <h3 style={{ textAlign: 'center' }}>Sign In</h3> */}
                    {
                        <Formik
                            onSubmit={(values) => onsubmitSignIn(values)}
                            validationSchema={validSchema}
                            enableReinitialize
                            initialValues={{
                                email: '',
                                password: '',
                            }}
                        >
                            {(formik) => (
                                <Form onSubmit={formik.handleSubmit} style={{ padding: '1rem' }} id="login">
                                    {loginError ? (
                                        <Alert variant="danger" className="rounded">Incorrect Email or Password</Alert>
                                    ) : ('')}
                                    <Form.Group controlId="email" as={Col} hasValidation>
                                        <Form.Label className="form__label">Email</Form.Label>
                                        <Form.Control
                                            className="p-3 rounded-0"
                                            type="text"
                                            name="email"
                                            placeholder="Please Enter Your Phone Number or Email"
                                            value={formik.values.email}
                                            onChange={formik.handleChange}
                                            onBlur={formik.handleBlur}
                                            isValid={formik.touched.email && !formik.errors.email}
                                            isInvalid={formik.touched.email && formik.errors.email}
                                        />

                                        <Form.Control.Feedback type="invalid">
                                            {formik.errors.email}
                                        </Form.Control.Feedback>
                                    </Form.Group>

                                    <Form.Group controlId="password" as={Col} className="isValidHide" hasValidation>
                                        <Form.Label className="form__label">Password</Form.Label>

                                        <Form.Control
                                            className="p-3 rounded-0 "
                                            // style={{backG}}
                                            type="password"
                                            name="password"
                                            placeholder="Please Enter Your Password"
                                            value={formik.values.password}
                                            onChange={formik.handleChange}
                                            onBlur={formik.handleBlur}
                                            isValid={formik.touched.password && !formik.errors.password}
                                            isInvalid={formik.touched.password && formik.errors.password}
                                        />
                                        <Form.Control.Feedback type="invalid">
                                            {formik.errors.password}
                                        </Form.Control.Feedback>
                                        <div style={{ display: 'flex', justifyContent: 'space-between', marginTop: '2.5rem' }}>
                                            <Form.Check
                                                type='checkbox'
                                                label='Remember me'
                                                className='section-signin__forgotLabel mt-3'
                                            // id={`disabled-default-${type}`}
                                            />
                                            <Button className="button section-signin__login-btn button btn-block rounded-0" type="submit" style={{ width: '11rem' }} form="login">
                                                {isLoading ? (
                                                    <div>
                                                        <Spinner animation="border" size="sm" />
                                                        <span style={{ marginLeft: '10px' }}>Sign In</span>
                                                    </div>
                                                ) :
                                                    <span>Sign In</span>
                                                }
                                            </Button>

                                        </div>
                                        <div style={{ display: 'flex', justifyContent: 'space-between', marginTop: '1rem' }}>
                                            <Link to='#' className="section-signin__forgotLabel mt-3" onClick={() => setResetPasswordModal(true)} style={{ textDecoration: 'none' }}>Forgot Password?</Link>
                                            <Link className="section-signin__forgotLabel mt-3" to='/signup' style={{ textDecoration: 'none' }}>Register Now</Link>
                                        </div>
                                    </Form.Group>
                                    <Form.Group as={Col} style={{ display: "flex", justifyContent: "center", alignItems: "center" }}>
                                        <StyledFirebaseAuth className="firebase-ui-cus" uiConfig={uiConfig} firebaseAuth={firebase.auth()} />
                                    </Form.Group>
                                </Form>
                            )
                            }
                        </Formik>
                    }
                </Col>
            </Row>
            <Modal
                show={resetPasswordModal}
                onHide={handleResetPasswordModalClose}
                centered
                size="lg"
            >
                <Modal.Header closeButton>
                    <Modal.Title>
                        Reset Password
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    {
                        resetPasswordSuccessAlert ?
                            <Alert variant="success" className="rounded">A password reset link has been sent to your
                                email address</Alert>
                            : ""
                    }
                    {
                        resetPasswordDangerAlert ?
                            <Alert variant="danger" className="rounded">No user registered with with email</Alert>
                            : ""
                    }
                    <Formik
                        onSubmit={(values, { resetForm }) => {
                            onResetPasswordEmailSubmit(values, resetForm)
                            // resetForm({ values: "" })
                        }}
                        validationSchema={resetPasswordvalidSchema}
                        enableReinitialize
                        initialValues={{
                            email: ''
                        }}
                    >
                        {(formik) => (
                            <Form onSubmit={formik.handleSubmit} id="resetPasswordEmail">
                                {resetPasswordSuccessAlert ?
                                    ""
                                    :
                                    <Form.Group controlId="email" as={Col} hasValidation>
                                        <Form.Label className="form__label">Email</Form.Label>
                                        <Form.Control
                                            className="p-3 rounded-0"
                                            type="text"
                                            name="email"
                                            placeholder="Please Enter Your Email"
                                            value={formik.values.email}
                                            onChange={formik.handleChange}
                                            onBlur={formik.handleBlur}
                                            isValid={formik.touched.email && !formik.errors.email}
                                            isInvalid={formik.touched.email && formik.errors.email}
                                        />
                                        <Form.Control.Feedback type="invalid">
                                            {formik.errors.email}
                                        </Form.Control.Feedback>
                                    </Form.Group>
                                }

                            </Form>
                        )}
                    </Formik>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleResetPasswordModalClose}>
                        Close
                    </Button>
                    <Button className="button" type="submit" form="resetPasswordEmail">
                        {isLoadingResetPassword ? (
                            <Spinner animation="border" size="sm" />
                        ) : ('')}
                        <span className={`${isLoading ? 'ml-2' : ''}`}>Submit</span>
                    </Button>
                </Modal.Footer>
            </Modal>
        </Container >

    )
}

export default SignIn;

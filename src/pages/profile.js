import React, { useState, useEffect, useContext } from 'react';
import { Row, Col, Form, Container, Button, Alert, Tabs, Tab, Spinner, Modal } from 'react-bootstrap';
import { Link, useHistory } from 'react-router-dom';
import { Formik } from 'formik';
import * as Yup from 'yup';
import api from './../api/index';
import { AuthContext } from '../contexts/auth';
import userAvatar from "../images/iconfinder_user.png";
import jwt_decode from "jwt-decode";
import { updateUserPassword } from '../store/actions/userActions'
import { useDispatch, useSelector } from 'react-redux';


const Profile = () => {
    const { isLogin, loginSuccess, setProfile, isProfileImg, profile, setIsProfileImg } = useContext(AuthContext);

    const validSchemaProfile = Yup.object().shape({
        // email: Yup.string().email().required("required"),
        name: Yup.string(),
        phone: Yup.string(),
    });

    const validSchemaChangePassword = Yup.object().shape({
        currentPassword: Yup.string().required("required"),
        password: Yup.string().required("required"),
        confirmPassword: Yup.string().oneOf([Yup.ref("password"), null], "Passwords must match").required("required"),
    });



    const dispatch = useDispatch();
    const [isLoading, setIsLoading] = useState(false);
    const [selectPic, setSelectPic] = useState(false);
    const [isLoadingChangePassword, setIsLoadingChangePassword] = useState(false);
    const [profileUpdateSuccess, setProfileUpdateSuccess] = useState(false);
    const [passwordUpdateSuccess, setPasswordUpdateSuccess] = useState(false);
    const [passwordUpdateDanger, setPasswordUpdateDanger] = useState(false);

    const [pp, setPP] = useState('');
    const [initialValues, setInitialValues] = useState({
        name: '',
        email: '',
        phone: '',
        photo: '',
    });


    const handleUpdateUserProfile = async (values) => {
        setIsLoading(true);
        try {
            if (selectPic) {
                console.log("one is called")
                const formData = new FormData();
                for (const key in values) {
                    if (Array.isArray(values[key])) {
                        formData.append(key, JSON.stringify(values[key]));
                    } else {
                        if (values[key] !== null)
                            formData.append(key, values[key])
                    }
                }
                formData.append('photo', initialValues.photo);
                console.log("fORMdATA", formData);

                const updatedUser = await api.patch("/users/updateMe", formData);
                console.log("res", updatedUser);
                setIsLoading(false);
                setProfileUpdateSuccess(true);
                setSelectPic(false)
                setProfileuser();
            }
            else {
                console.log("two is called");
                console.log('values', values);
                console.log('values2', initialValues);
                const updatedUser = await api.patch("/users/updateMe", values);
                console.log("res", updatedUser);
                setIsLoading(false);
                setProfileUpdateSuccess(true);
                setProfileuser();
            }
        } catch (error) {
            console.log(error);
            setIsLoading(false);
        }

    }

    const setProfileuser = async () => {
        console.log('i am called')
        const token2 = localStorage.getItem('token');
        var decoded2 = jwt_decode(token2);
        const res = await api.get(`/users/${decoded2.id}`);
        const { photo, name, email, phone } = res.data.doc
        setInitialValues({ ...initialValues, name: name, email: email, phone: phone, photo: photo });
        setPP(photo);
    }

    const updatePassword = async (values, resetForm) => {
        try {
            setIsLoadingChangePassword(true);
            await dispatch(updateUserPassword(values));
            setIsLoadingChangePassword(false);
            setPasswordUpdateSuccess(true);
            setPasswordUpdateDanger(false)
            resetForm({ values: '' })
        } catch (error) {
            console.log(error)
            setIsLoadingChangePassword(false);
            setPasswordUpdateDanger(true)
            setPasswordUpdateSuccess(false);
            resetForm({ values: '' })
        }

    }

    const handleFormikFileChange = (e, formik) => {
        const file = e.target.files[0];
        formik.handleChange(e);
        console.log("file", file)
        setInitialValues({ ...initialValues, photo: file });
        setSelectPic(true)
    }

    useEffect(() => {
        setProfileuser();
        window.scrollTo(0, 0);
    }, [])
    return (
        <Container className="section-signIn shadow" style={{ padding: '2rem', width: '70rem' }}>
            <div style={{ width: '20%', height: '16%', marginBottom: '0.1rem' }} className='shadow'>
                {isProfileImg ? (
                    <img
                        className="navbar__user-img"
                        src={pp}
                        alt="profile image"
                        style={{ width: "100%", height: "100%" }}
                    />
                ) : (
                    <img
                        className="navigation__user-img"
                        src={userAvatar}
                        alt="profile img"
                        style={{ width: "100%", height: "100%" }}
                    />
                )}

            </div>
            <Formik
                onSubmit={(values) => handleUpdateUserProfile(values)}
                validationSchema={validSchemaProfile}
                enableReinitialize
                initialValues={initialValues}
            >
                {(formik) => (
                    <Form onSubmit={formik.handleSubmit} id="profile-pic">
                        <Form.Group as={Col} controlId="photo" hasValidation>
                            {/* <Form.Label className="form__label">Profile Pic</Form.Label> */}
                            <Form.Control
                                className="rounded-0"
                                type="file"
                                name="photo"
                                // value={formik.values.photo}
                                onChange={e => handleFormikFileChange(e, formik)}
                            />
                        </Form.Group>
                    </Form>
                )}
            </Formik>

            <Row style={{ padding: '0rem 2rem' }}>
                <Col md={5} style={{ marginTop: '2.4rem' }}  >
                    {profileUpdateSuccess ? (
                        <Alert variant="success" className="rounded">updated Successfully</Alert>
                    ) : ('')}
                    <div>
                        <Formik
                            onSubmit={(values) => handleUpdateUserProfile(values)}
                            validationSchema={validSchemaProfile}
                            enableReinitialize
                            initialValues={initialValues}
                        >
                            {(formik) => (
                                <Form onSubmit={formik.handleSubmit} id="profile">

                                    <Form.Group controlId="name" as={Col} hasValidation>
                                        <Form.Label className="form__label">Name</Form.Label>
                                        <Form.Control
                                            className="p-3 rounded-0"
                                            type="text"
                                            name="name"
                                            placeholder="Please Enter Your Name"
                                            value={formik.values.name}
                                            onChange={formik.handleChange}
                                            onBlur={formik.handleBlur}
                                            isValid={formik.touched.name && !formik.errors.name}
                                            isInvalid={formik.touched.name && formik.errors.name}
                                        />

                                        <Form.Control.Feedback type="invalid">
                                            {formik.errors.name}
                                        </Form.Control.Feedback>
                                    </Form.Group>

                                    <Form.Group controlId="email" as={Col} className="isValidHide" hasValidation>
                                        <Form.Label className="form__label">Email</Form.Label>

                                        <Form.Control
                                            className="p-3 rounded-0 "
                                            // style={{backG}}
                                            type="text"
                                            name="email"
                                            disabled
                                            placeholder="Please Enter Your Email"
                                            value={formik.values.email}
                                            onChange={formik.handleChange}
                                            onBlur={formik.handleBlur}
                                            isValid={formik.touched.email && !formik.errors.email}
                                            isInvalid={formik.touched.email && formik.errors.email}
                                        />
                                        <Form.Control.Feedback type="invalid">
                                            {formik.errors.email}
                                        </Form.Control.Feedback>
                                    </Form.Group>
                                    <Form.Group as={Col} style={{ display: "flex", justifyContent: "center", alignItems: "center" }}>

                                    </Form.Group>

                                    <Form.Group controlId="phone" as={Col} className="isValidHide" hasValidation>
                                        <Form.Label className="form__label">Phone</Form.Label>
                                        <Form.Control
                                            className="p-3 rounded-0 "
                                            // style={{backG}}
                                            type="text"
                                            name="phone"
                                            placeholder="Please Enter Your Phone"
                                            value={formik.values.phone}
                                            onChange={formik.handleChange}
                                            onBlur={formik.handleBlur}
                                            isValid={formik.touched.phone && !formik.errors.phone}
                                            isInvalid={formik.touched.phone && formik.errors.phone}
                                        />
                                        <Form.Control.Feedback type="invalid">
                                            {formik.errors.phone}
                                        </Form.Control.Feedback>
                                        <Button className="button section-signin__login-btn button btn-block rounded-0" type="submit" style={{ width: '11rem', marginTop: '1.5rem' }} form="profile">
                                            {isLoading ? (
                                                <div>
                                                    <Spinner animation="border" size="sm" />
                                                    <span style={{ marginLeft: '10px' }}>Update</span>
                                                </div>
                                            ) :
                                                <span>Update</span>
                                            }
                                        </Button>
                                    </Form.Group>
                                </Form>
                            )
                            }
                        </Formik>

                    </div>
                </Col>
                <Col md={5} >
                    <div className='fss'>
                        <h2>Change Password</h2>
                    </div>
                    {passwordUpdateSuccess ? (
                        <Alert variant="success" className="rounded">Password updated Successfully</Alert>
                    ) : ('')}
                    {passwordUpdateDanger ? (
                        <Alert variant="danger" className="rounded">Incorrect Password</Alert>
                    ) : ('')}
                    <div>
                        <Formik
                            onSubmit={(values, { resetForm }) => updatePassword(values, resetForm)}
                            validationSchema={validSchemaChangePassword}
                            enableReinitialize
                            initialValues={{
                                currentPassword: '',
                                password: '',
                                confirmPassword: ''
                            }}
                        >
                            {(formik) => (
                                <Form onSubmit={formik.handleSubmit} id="changePassword">
                                    <Form.Group controlId="currentPassword" as={Col} hasValidation>
                                        <Form.Label className="form__label">Old Password</Form.Label>
                                        <Form.Control
                                            className="p-3 rounded-0"
                                            type="password"
                                            name="currentPassword"
                                            placeholder="Please Enter Old Password"
                                            value={formik.values.currentPassword}
                                            onChange={formik.handleChange}
                                            onBlur={formik.handleBlur}
                                            isValid={formik.touched.currentPassword && !formik.errors.currentPassword}
                                            isInvalid={formik.touched.currentPassword && formik.errors.currentPassword}
                                        />

                                        <Form.Control.Feedback type="invalid">
                                            {formik.errors.currentPassword}
                                        </Form.Control.Feedback>
                                    </Form.Group>

                                    <Form.Group controlId="password" as={Col} className="isValidHide" hasValidation>
                                        <Form.Label className="form__label">New Password</Form.Label>
                                        <Form.Control
                                            className="p-3 rounded-0 "
                                            // style={{backG}}
                                            type="password"
                                            name="password"
                                            placeholder="Please Enter New Password"
                                            value={formik.values.password}
                                            onChange={formik.handleChange}
                                            onBlur={formik.handleBlur}
                                            isValid={formik.touched.password && !formik.errors.password}
                                            isInvalid={formik.touched.password && formik.errors.password}
                                        />
                                        <Form.Control.Feedback type="invalid">
                                            {formik.errors.password}
                                        </Form.Control.Feedback>
                                    </Form.Group>
                                    <Form.Group as={Col} style={{ display: "flex", justifyContent: "center", alignItems: "center" }}>

                                    </Form.Group>

                                    <Form.Group controlId="confirmPassword" as={Col} className="isValidHide" hasValidation>
                                        <Form.Label className="form__label">Confirm Password</Form.Label>
                                        <Form.Control
                                            className="p-3 rounded-0 "
                                            // style={{backG}}
                                            type="password"
                                            name="confirmPassword"
                                            placeholder="Please Enter Confirm Password"
                                            value={formik.values.confirmPassword}
                                            onChange={formik.handleChange}
                                            onBlur={formik.handleBlur}
                                            isValid={formik.touched.confirmPassword && !formik.errors.confirmPassword}
                                            isInvalid={formik.touched.confirmPassword && formik.errors.confirmPassword}
                                        />
                                        <Form.Control.Feedback type="invalid">
                                            {formik.errors.confirmPassword}
                                        </Form.Control.Feedback>
                                        <Button className="button section-signin__login-btn button btn-block rounded-0" type="submit" style={{ width: '11rem', marginTop: '1.5rem' }} form="changePassword">
                                            {isLoadingChangePassword ? (
                                                <div>
                                                    <Spinner animation="border" size="sm" />
                                                    <span style={{ marginLeft: '10px' }}>Submit</span>
                                                </div>
                                            ) :
                                                <span>Submit</span>
                                            }
                                        </Button>
                                    </Form.Group>
                                </Form>
                            )
                            }
                        </Formik>

                    </div>
                </Col>

                {/* <Col md={7}>
                </Col> */}

            </Row>

        </Container >

    )
}

export default Profile;

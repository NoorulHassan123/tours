import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import './styles/main.scss';
import { Provider } from 'react-redux';
import store from './store/index';
import ContextProvider from './contexts';
import firebase from 'firebase';
// import firebase from 'firebase/compat/app';

if (!firebase.apps.length) {
  firebase.initializeApp({
    apiKey: 'AIzaSyCK5Wvyhd3VTe7gBmtJNOiQp8nXBy3HK3A',
    authDomain: 'tours-c7067.firebaseapp.com'
  });
} else {
  firebase.app(); // if already initialized, use that one
}

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <ContextProvider >
        <App />
      </ContextProvider>
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals


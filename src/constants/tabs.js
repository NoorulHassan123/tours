export default {
    HOME: "home",
    ABOUT: 'about',
    PHILOSOPHY: 'philosophy',
    SERVICES: 'services',
    WORK: 'work'
}
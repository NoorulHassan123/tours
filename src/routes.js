import React, { useContext } from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import { Modal, Button } from 'react-bootstrap';
import NavBar from './components/NavBar';
import Home from './pages/home';
import TourDetail from './pages/tourDetail';
import SignIn from './pages/signIn';
import SignUp from './pages/signUp';
import Footer from './components/Footer';
import Profile from './pages/profile';
import ResetPassword from './pages/resetPassword';

const Routes = () => {
    return (
        <>
            <NavBar />
            <Switch>
                <Route exact path="/" component={Home} />
                <Route exact path="/tours/:id" component={TourDetail} />
                <Route exact path="/signin" component={SignIn} />
                <Route exact path="/signup" component={SignUp} />
                <Route exact path="/profile" component={Profile} />
                <Route exact path="/resetPassword/:resetPasswordToken" component={ResetPassword} />
                <Redirect to="/" />
            </Switch>
            <Footer />
            {/* <Footer /> */}
        </>
    )
}

export default Routes;